package com.varn.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Servlet implementation class getEventList
 */
@WebServlet("/getEventList")
public class getEventList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	Connection conn = null;
	PreparedStatement pstmt = null;
	
	String jdbcUrl = "jdbc:mysql://localhost:3306/hcc_web";
	String dbId = "deric";
	String dbPwd = "2qkr&thwkd";
	String mysqlDriver = "com.mysql.jdbc.Driver";
	
	JSONArray jsonArray;
	
	public void connect() { // DB 연결 메소드
		try {
			Class.forName(this.mysqlDriver); // jdbc드라이버를 DriverManager에 등록한다.
			System.out.println(this.jdbcUrl);
			this.conn = DriverManager.getConnection(this.jdbcUrl, dbId, dbPwd); // Connection 객체를 얻어온다.
		} catch (Exception e) {
			e.printStackTrace(); // 예외 처리
		}
	}
	
	public void disconnect() { // DB 연결종료 메소드
		if (pstmt != null)
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		if (conn != null)
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
	public JSONArray getDeviceData() {
		
		connect();
		
		String sql = "select * from eventlist";
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			ResultSet rs = pstmt.executeQuery();
			
			jsonArray = new JSONArray();
			
			while(rs.next()) {
				JSONObject object = new JSONObject();
				object.put("event", rs.getString("event"));
				jsonArray.add(object);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}
		
		return jsonArray;
	}
	
	
    public getEventList() {
        super();
        // TODO Auto-generated constructor stub
    }


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=utf-8");
		
		JSONArray jsonArray = getDeviceData();
		System.out.println(jsonArray);
		if(jsonArray != null) {
			System.out.println(jsonArray);
			response.getWriter().print(jsonArray);
		}
	}

}
