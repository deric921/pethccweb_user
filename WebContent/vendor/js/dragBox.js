
	// Drag & Drop (BOX)

	const fill1 = document.querySelector('.fill1');
	const fill2 = document.querySelector('.fill2');
	const fill3 = document.querySelector('.fill3');
	const fill4 = document.querySelector('.fill4');
	
	const empties = document.querySelectorAll('.empty');
	
	const dragstarts = null;
	
	// Fill Listeners
    fill1.addEventListener('dragstart', dragStart);
	fill1.addEventListener('dragend', dragEnd1);
	
	fill2.addEventListener('dragstart', dragStart);
	fill2.addEventListener('dragend', dragEnd2);
	
	fill3.addEventListener('dragstart', dragStart);
	fill3.addEventListener('dragend', dragEnd3);
	
	fill4.addEventListener('dragstart', dragStart);
	fill4.addEventListener('dragend', dragEnd4);
	

	// Loop through empty and call drag events
	for(const empty of empties){   	
		empty.addEventListener('dragover', dragOver);
		empty.addEventListener('dragenter',dragEnter);
		empty.addEventListener('dragleave',dragLeave);
		empty.addEventListener('drop',dragDrop);
	}

	
	// Drag Functions
	function dragStart(){
		this.className += ' hold';
		setTimeout(() => (this.className = 'invisible'), 0);
	}

	function dragEnd1(){
		this.className = 'fill1';
	}
	function dragEnd2(){
		this.className = 'fill2';
	}
	function dragEnd3(){
		this.className = 'fill3';
	}
	function dragEnd4(){
		this.className = 'fill4';
	}
	
	function dragOver(e) {
		e.preventDefault();
	}	

	function dragEnter(e){
		e.preventDefault();
		this.className += ' hovered';
	}

	function dragLeave(){
		this.className = 'empty';
	}

	function dragDrop(){  // 분기 fiil1~4
		this.className = 'empty';
		this.append(fill1);
		
			
	}


