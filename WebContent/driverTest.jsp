<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
 
<%//JSP에서 JDBC의 라이브러리 클래스를 사용하기 위해 java.sql 패키지 import %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>JDBC driver Test</h2>
	<%
		Connection conn = null;
		try{
			String jdbcUrl = "jdbc:mysql://localhost:3306/hcc_web?allowMultiQueries=true"; //DB 명을 포함한 URL
			String dbId = "deric"; // DB명에 해당하는 사용자 계정
			String dbPwd = "2qkr&thwkd"; //DB명에 해당하는 사용자 패스워드
			String mariaDriver = "org.mariadb.jdbc.Driver";//mariaDB connector driver
			String mysqlDriver = "com.mysql.jdbc.Driver"; //mysql connector driver
			
			Class.forName(mysqlDriver); // jdbc드라이버를 DriverManager에 등록한다.
			conn = DriverManager.getConnection(jdbcUrl,dbId,dbPwd);  // Connection 객체를 얻어온다.
			out.println("제대로 연결되었습니다."); // 커넥션이 연결되면 수행된다.		
		}catch(Exception e){ //예외 발생시 예외상황 처리
			e.printStackTrace(); //예외가 발생하면 추적하여 예외 발생 과정을 출력한다.
		}
	
	%>
</body>
</html>