<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Box_test</title>


<!-- Theme CSS - Includes Bootstrap -->
<link href="vendor/bootstrap-4.3.1/bootstrap.css" rel="stylesheet" />

<!-- Custom Style CSS -->
<link href="css/reset.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery-3.4.1/jquery.min.js"></script>
<script src="vendor/bootstrap-4.3.1/bootstrap.min.js"></script>



</head>
<body>
	<div class="container">
		<div class="row mt-4">
			<div class="col-lg-6 col-sm-6 mt-3">
				<h3>Activity</h3>
				<select class="form-control selectbar mt-4" id="selectBox1">
					<option value="Touch">Touch</option>
					<option value="Jab">Jab</option>
					<option value="Punch">Punch</option>
					<option value="Kick">Kick</option>
					<option value="Drop">Drop</option>
					<option value="Catch">Catch</option>
					<option value="Juggle">Juggle</option>
				</select>
			</div>
			<div class="col-lg-6 col-sm-6 mt-3">
				<h3>Event</h3>
				<select class="form-control selectbar mt-4" id="selectBox2">
				</select>
			</div>
			<div class="col-lg-12 col-sm-6 mt-3">
				<button class="btn btn-1" onClick="setDatas()">submit</button>
			</div>
		</div>
		<div class="row mt-4 ">
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      			<div class="fill1" draggable="true">
	      				<div class="img2"></div>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      			<div class="fill2" draggable="true">
	      				<div class="img1"></div>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      			<div class="fill3" draggable="true">
	      				<div class="img3"></div>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      			<div class="fill4" draggable="true">
	      				<div class="img4"></div>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      			
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      		</div>
	      	</div>
	   	</div>
		<div class="row mt-4 "> 
			<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      			
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      		
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      		
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      		
	      		</div>
	      	</div>
	      	<div class="col-lg-2 col-sm-2 mt-3 ">
	      		<div class="empty">
	      		
	      		</div>
	      	</div>	
		</div>
<!-- 	    <div class="row mt-4 ">
	      <div class="col-lg-6 col-sm-6 mt-3 ">
	      	<div class="empty">
	      		<div class="fill" draggable="true"></div>
	      	</div>
	      </div>
	      <div class="col-lg-6 col-sm-6 mt-3 ">
	      	<div class="empty"></div>
	      </div>
	      <div class="col-lg-6 col-sm-6 mt-3 ">
	      	<div class="empty"></div>
	      </div>
	      <div class="col-lg-6 col-sm-6 mt-3 ">
	     	<div class="empty"></div>
	      </div>
	      <div class="col-lg-6 col-sm-6 mt-3 ">
	        <div class="empty"></div>
	      </div>
	      <div class="col-lg-6 col-sm-6 mt-3 ">
	        <div class="empty"></div>
	      </div>
	    </div> -->
	    
     
	<script src="vendor/js/dragBox.js"></script>
</body>
<script>

	
	
	$(document).ready(function(){
		getEventList();
	});
	var event = [];
	
	// selectbar 데이터 가져오기
	function getEventList(){
		$.ajax({
			url: "../pethccweb_user/getEventList",
			type: 'POST',
			dataType:'json',
			data: {

			},
			success: function(data){
				setEventList(data);
				event = data;
			},
			error:function(jqXHR, status, er){
				alert(jqXHR.responseText);
			}
		})
	}

	// 셀렉트 박스 만들기 (Event)
	function setEventList(data){
		var selectBox = $('#selectBox2');
		for (var i=0; i<data.length;i++){
			selectBox.append("<option value='"+data[i].event+"'>"+data[i].event+"</option>");
		}
	}

	function setDatas() {
		var activity = $('#selectBox1').val();  // 셀렉트박스 1값
		var event = $('#selectBox2').val();		// 셀렉트박스2값

		$.ajax({
			url:"../pethccweb_user/setDeviceEvent",
			type:'Post',
			dataType:'json',
			data:{
				activity : activity,
				event : event
			},
			success: function(data){

			},
			error:function(jqXHR, status, er){
				alert(jqXHR.responseText);
			}
		})

	}
</script>


</html>
